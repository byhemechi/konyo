import 'dart:math';
import 'colours.dart';
import 'dart:io';

const _defaultSize = Point(6, 12);

class Board {
  List<List<Colours>> _values;
  Point<int> size = Point(6, 12);
  int cellWidth;

  Board({this.size = _defaultSize, this.cellWidth = 4}) {
    assert(cellWidth & 1 == 0, "cellWidth must be a multiple of 2");
    _values = List.generate(
        size.y, (_) => new List.generate(size.x, (i) => Colours.reset));
  }

  set(Point position, Colours value) {
    _values[position.y][position.x] = value;
  }

  render() {
    stdout.write("┌");
    stdout.write("─" * (size.x * cellWidth));
    print("┐");
    
    for (int y = 0; y < size.y; y++) {
      for (int i = 0; i < cellWidth / 2; ++i) {
        stdout.write("│");
        int x = 0;
        _values[y].forEach((i) {
          stdout.write(i.background + " " * (cellWidth));
          x++;
        });
        print("\x1B[0m│");
      }
    }
    stdout.write("╰");
    stdout.write("─" * (size.x * cellWidth));
    stdout.write("┘" "\x1B[${(size.y * cellWidth / 2 + 1).floor()}A" "\x1B[${((size.x + 1) * cellWidth).floor()}D");
  }

  tick() {
    for(int y = size.y - 1; y >= 0; --y) {
      for(int x = 0; x < size.x; ++x) {
        if(y + 1 < size.y && _values[y + 1][x] == Colours.reset) {
          _values[y + 1][x] = _values[y][x];
          _values[y][x] = Colours.reset;
        }
      }
    }
  }
}
