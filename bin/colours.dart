enum Colours {reset, red, green, yellow, blue}

Map<Colours, int> _values = {
  Colours.reset: 9,
  Colours.red: 1,
  Colours.green: 2,
  Colours.yellow: 3,
  Colours.blue: 4,
};

extension ColourValues on Colours {
  String get foreground => "\x1B[3${_values[this]}m";
  String get background => "\x1B[4${_values[this]}m";
}