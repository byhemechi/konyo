import 'dart:math';

import "./colours.dart";
import 'dart:io';

import 'board.dart';

main() {
  Board game = Board();
  bool quit = false;

  game.set(Point(3, 3), Colours.red);
  game.set(Point(2, 2), Colours.yellow);

  stdout.write("\x1B[?25l");
  while (!quit) {
    game.tick();
    game.render();
    sleep(Duration(milliseconds: 32));
  }
}
